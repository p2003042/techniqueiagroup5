#les regles no defeasible sont avec des fleches simple et les regles defeasable sont avec des flelche epaise

#contraposee de a->b est !a U b et !a->!b  
#contraposee de a,b->c est !a,b->!c et a,!b->!c
#TODO faire en sorte que le literal de reference dans Rule soit crée automatiquement par incrementation d'un compteur globale
#TODO faire en sorte que la cration des arguments soit automatiquement faite en fonction des regles
#les 18 arguments doivent etre déduite des 12 règles dans l'image de la consigne
#les argument devrait pouvoir generer les sous argument nessesaires : genre 
#TODO regler l'affigchange des regle avec juste une conclusion
#TODO faire en sorte que ce l'affichage du character negatif se fasse automatiqument au lieu d'etre définit dans le name

#il faut automatiquement rajouter a les regles generer par getcontraposer sur les regles strict dans l'ansambe des regles 

class Literal:
    def __init__(self, name, is_negative=False):
        self.name = name
        self.is_negative = is_negative

    def __repr__(self):
        if self.is_negative:
            return f"¬{self.name}"
        else:
            return self.name

    def __eq__(self, other):
        return self.name == other.name and self.is_negative == other.is_negative


    def __hash__(self):
        return hash((self.name, self.is_negative))


class Rule:
    _rule_count = 0

    def __init__(self, premises, conclusion, is_defeasible=False, reference=None):
        self.premises = premises
        self.conclusion = conclusion
        self.is_defeasible = is_defeasible
        if reference is None:
            Rule._rule_count += 1
            self.reference = Literal(f"r{Rule._rule_count}")
        else:
            self.reference = reference

    def __repr__(self):
        premises_str = ", ".join(map(str, self.premises))
        if premises_str:
            if self.is_defeasible:
                premises_str += " => "
            else:
                premises_str += " -> "
        return f"[{self.reference}] {premises_str}{self.conclusion}"

    def __eq__(self, other):
        return (self.premises == other.premises and
                self.conclusion == other.conclusion and
                self.is_defeasible == other.is_defeasible and
                self.reference == other.reference)


    def __hash__(self):
        return hash((tuple(self.premises), self.conclusion, self.is_defeasible, self.reference))






class Argument:
    def __init__(self, top_rule, direct_sub_arguments, name):
        self.top_rule = top_rule
        self.direct_sub_arguments = direct_sub_arguments
        self.name = name

    def __repr__(self):
        direct_sub_args_str = ", ".join(map(str, self.direct_sub_arguments))
        return f"{self.name}: {self.top_rule} | {direct_sub_args_str}"

    def __eq__(self, other):
        return (self.top_rule == other.top_rule and
                self.direct_sub_arguments == other.direct_sub_arguments and
                self.name == other.name)



def create_contraposition_rules(rule):
    contraposition_rules = []

    for premise in rule.premises:
        new_premises = rule.premises.copy()  # Copy existing premises
        new_premises.remove(premise)  # Remove the current premise
        new_premises.append(Literal(rule.conclusion.name, is_negative=not rule.conclusion.is_negative))  # Add the conclusion with opposite polarity

        new_conclusion = Literal(premise.name, is_negative=not premise.is_negative)  # Get the opposite polarity for the premise

        contraposition_rules.append(Rule(new_premises, new_conclusion))

    return contraposition_rules




# Define the base set of rules
base_rules = {
    Rule([], Literal("a")),
    Rule([Literal("b"), Literal("d")], Literal("c")),
    Rule([Literal("c", is_negative=True)], Literal("d")),
    Rule([Literal("a")], Literal("d", is_negative=True), is_defeasible=True),
    Rule([], Literal("b"), is_defeasible=True),
    Rule([], Literal("c", is_negative=True), is_defeasible=True),
    Rule([], Literal("d"), is_defeasible=True),
    Rule([Literal("c")], Literal("e"), is_defeasible=True),
    Rule([Literal("c", is_negative=True)], Literal("r4", is_negative=True), is_defeasible=True)
}

# Apply create_contraposition_rules function to strict rules and add them to base_rules
for rule in base_rules.copy():  # Iterate over a copy to avoid modifying while iterating
    if not rule.is_defeasible:
        contraposition_rules = create_contraposition_rules(rule)
        base_rules.update(contraposition_rules)

# Display all rules in base_rules sorted alphabetically
for rule in sorted(base_rules, key=lambda x: str(x)):
    print(rule)







# # Example usage:

# # Original rules
# r1 = Rule([Literal("a")], Literal("b"))
# r2 = Rule([Literal("a"), Literal("b")], Literal("c"))

# # Creating contraposition rules
# contraposition_rules_r1 = create_contraposition_rules(r1)
# contraposition_rules_r2 = create_contraposition_rules(r2)

# # Displaying contraposition rules
# print("Original Rule 1:", r1)
# print("Contraposition Rules for Rule 1:")
# for rule in contraposition_rules_r1:
#     print(rule)

# print("\nOriginal Rule 2:", r2)
# print("Contraposition Rules for Rule 2:")
# for rule in contraposition_rules_r2:
#     print(rule)






# # Example usage:

# # Creating literals
# x = Literal("x")
# not_y = Literal("y", is_negative=True)

# # Creating rules
# r1 = Rule([x, not_y], Literal("e"),is_defeasible=False, reference=None)
# r2 = Rule([x], not_y, is_defeasible=True, reference=None)

# # Creating arguments
# A1 = Argument(r2, [], "A1")
# A4 = Argument(r2, [], "A4")
# A2 = Argument(r1, [A1], "A2")
# A3 = Argument(r1, [ A1, A4], "A3")

# # Printing
# print(x)  # Output: x
# print(not_y)  # Output: ¬y
# print(r1)  # Output: x: x, ¬y ⇒ e
# print(r2)  # Output: ¬y: ¬y
# print(A1)  # Output: A1: ¬y: ¬y | 
# print(A2)  # Output: A2: x: x, ¬y ⇒ e | A1: ¬y: ¬y
# print(A3)  # 


# # Example usage:

# # Creating literals
# x = Literal("x")
# not_y = Literal("y", is_negative=True)

# # Creating rules without explicit reference
# r1 = Rule([x, not_y], Literal("e"), reference=None)
# r2 = Rule([], not_y, is_defeasible=True, reference=None)

# # Printing
# print(r1)  # Output: r1: x, ¬y -> e
# print(r2)  # Output: r2: ¬y => 