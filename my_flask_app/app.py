import sys
import os
import matplotlib.pyplot as plt
import io
import base64
import multiprocessing  # Import the multiprocessing module for timeout handling
import traceback  # Import traceback for error handling

from flask import Flask, render_template, request

# Add the parent directory of your_module to the Python path
module_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'your_module'))
if module_path not in sys.path:
    sys.path.append(module_path)

# Now you can import from your_module
from your_module.rule_handler import base_rules, generate_arg, generate_rebuts, deffeat, display_rebuts_grouped_by_conclusion, Argument, Rule, undercuts, h_categoriser_semantics
from your_module.rule_converter import parse_user_input, serialize_rules
from your_module.defeat_analysis import calculate_attacks, generate_histogram

# Create a Flask app instance
app = Flask(__name__)

#TODO voir  a corrigée le fait que quand on lance plusieurs fois d'affiler le traitement des regles ne numéros des règles n'est pas réinitialisé


pref = {
     "r4":["r2","r7","r8","r9"],
     "r6":["r2","r7","r8","r9"]
     #"r1":["r2","r7","r8","r9","r3","r4","r5","r6","r10","r11","r12","r23","r24","r25","r26","r27","r28"]
}

TIMEOUT_DURATION = 3

# Function to execute long-running operation in a separate process
def execute_long_running_operation(user_input, queue):
    try:
        # converts the input into rules
        rule_from_string = parse_user_input(user_input)
        original_rule_from_string = rule_from_string.copy()

        # Execute the arg function to generate new arguments
        new_arguments = generate_arg(rule_from_string)
        original_new_arguments = new_arguments.copy()

        # Generation des attaques pour les afficher
        attacks = calculate_attacks(new_arguments)

        # Extract rule strings from new_arguments
        for arg in new_arguments:
            rule_from_string.append(str(arg.top_rule))

        # Generate rebuts for the new arguments
        all_rebuts = generate_rebuts(new_arguments)

        # Generate defeats
        result, defended_by = deffeat(new_arguments, pref)

        # Construct strings for printing defended_by
        defended_by_strings = []
        for defender, attackers in defended_by.items():
            for attacker in attackers:
                defended_by_strings.append(f"{defender} defended by : {attacker}")

        # Display rebuts grouped by conclusion
        rebut_groups = display_rebuts_grouped_by_conclusion(all_rebuts)

        # Argumentation framework for h_categoriser_semantics
        rule_from_string_for_h_cat = parse_user_input(user_input)

        new_arguments_for_h_cat = generate_arg(rule_from_string_for_h_cat)
        rebuts_for_h_cat = generate_rebuts(new_arguments_for_h_cat)
        undercut_for_h_cat = undercuts(new_arguments_for_h_cat)
        attacks_for_h_cat = rebuts_for_h_cat.union(undercut_for_h_cat)

        scores = h_categoriser_semantics(new_arguments_for_h_cat, attacks_for_h_cat)

        # Generate the histogram
        defeat_in_degree_counts = {}
        for defender, attackers in defended_by.items():
            in_degree = len(attackers)
            if in_degree not in defeat_in_degree_counts:
                defeat_in_degree_counts[in_degree] = 1
            else:
                defeat_in_degree_counts[in_degree] += 1

        fig, ax = plt.subplots(figsize=(10, 6))
        ax.bar(defeat_in_degree_counts.keys(), defeat_in_degree_counts.values(), color='skyblue')
        ax.set_xlabel('Defeat In-degree')
        ax.set_ylabel('Number of Arguments')
        ax.set_title('Histogram of Number of Arguments per Defeat In-degree')
        ax.grid(axis='y', linestyle='--', alpha=0.7)
        plt.tight_layout()

        img = io.BytesIO()
        plt.savefig(img, format='png')
        img.seek(0)
        plot_url = base64.b64encode(img.getvalue()).decode()

        queue.put((original_rule_from_string, original_new_arguments, defended_by_strings, attacks, defeat_in_degree_counts, plot_url, scores, rebut_groups))
    except Exception as e:
        traceback.print_exc()
        queue.put(None)
# Define the route for the homepage
@app.route('/', methods=['GET', 'POST'])
def index():
    # Reset the counters or IDs
    Argument._argument_count = 0
    Rule._rule_count = 0

    user_input = ""
    error_message = ""
    rule_from_string = []
    defended_by_strings = []
    rebut_groups = {}
    result = {}
    new_arguments = []
    attacks = {}
    all_attackers = []
    all_rebuts = []
    defended_by = {}
    original_rule_from_string = []
    original_new_arguments = []
    defeat_in_degree_counts = {}
    plot_url = ""
    scores = {}

    if request.method == 'POST':
        user_input = request.form['user_input']

        # Create a multiprocessing Queue
        queue = multiprocessing.Queue()

        # Create a separate process to execute the long-running operation
        process = multiprocessing.Process(target=execute_long_running_operation, args=(user_input, queue))
        process.start()

        # Wait for the process to finish or timeout
        process.join(TIMEOUT_DURATION)

        # If the process is still alive, terminate it
        if process.is_alive():
            process.terminate()
            process.join()

        # Retrieve the results from the queue
        if not queue.empty():
            original_rule_from_string, original_new_arguments, defended_by_strings, attacks, defeat_in_degree_counts, plot_url, scores, rebut_groups = queue.get()


    # return render_template('index.html', user_input=original_rule_from_string, rule_strings=original_new_arguments, rebut_groups=rebut_groups, all_defeats={}, defended_by_strings=defended_by_strings, attacks=attacks, defeat_in_degree_counts=defeat_in_degree_counts, plot_url=plot_url, error_message=error_message, scores=scores)
    return render_template('index.html', user_input=original_rule_from_string, rule_strings=original_new_arguments, rebut_groups=rebut_groups, all_defeats={}, defended_by_strings=defended_by_strings, attacks=attacks, defeat_in_degree_counts=defeat_in_degree_counts, plot_url=plot_url, error_message=error_message, scores=scores)

# Run the Flask app
if __name__ == '__main__':
    app.run(debug=True)
