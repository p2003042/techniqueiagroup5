import sys
import os

from flask import Flask, render_template, request

# Add the parent directory of your_module to the Python path
module_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'your_module'))
if module_path not in sys.path:
    sys.path.append(module_path)

# Now you can import from your_module
from your_module.rule_handler import base_rules, generate_arg, generate_rebuts, deffeat, display_rebuts_grouped_by_conclusion, Argument, Rule
from your_module.rule_converter import parse_user_input, serialize_rules
from your_module.defeat_analysis import plot_defeat_histogram



pref = {
     "r4":["r2","r7","r8","r9"],
     "r6":["r2","r7","r8","r9"]
     #"r1":["r2","r7","r8","r9","r3","r4","r5","r6","r10","r11","r12","r23","r24","r25","r26","r27","r28"]

   
}


#TODO handle error to print theme instead of crashing the page 


# Reset the counters or IDs
# Argument._argument_count = 0
# Rule._rule_count = 0


user_input = ""  # Initialize user_input variable

# if request.method == 'POST':
#     # Get user input from the form
#     user_input = request.form['user_input']
    # Here you can handle the user input if needed

    
# converts the input into rules
rule_from_string = []
rule_from_string = parse_user_input(user_input)
original_rule_from_string = rule_from_string.copy()


# Execute the arg function to generate new arguments
new_arguments = []
new_arguments = generate_arg(rule_from_string)
original_new_arguments = new_arguments.copy()


# Extract rule strings from new_arguments
for arg in new_arguments:
    rule_from_string.append(str(arg.top_rule))

# new_arguments = list(set(new_arguments))
# {self.name}: {self.top_rule} | {direct_sub_args_str}


# Generate rebuts for the new arguments
all_rebuts = []
all_rebuts = generate_rebuts(new_arguments)

# Generate defeats
result = {}
all_attackers = []
defended_by = {} # defender, attackers in defended_by.items()
# result, all_attackers = deffeat(new_arguments, pref)
result, defended_by  = deffeat(new_arguments, pref)


# Construct strings for printing defended_by
defended_by_strings = []
for defender, attackers in defended_by.items():
    for attacker in attackers:
        defended_by_strings.append(f"{defender} défendu par : {attacker}")



# Display rebuts grouped by conclusion
rebut_groups = []
rebut_groups = display_rebuts_grouped_by_conclusion(all_rebuts)


plot_defeat_histogram(result)

# print("All Defeats:", all_defeats)


# Render the index.html template with the results  
# return render_template('index.html', user_input=user_input, rule_from_string=rule_from_string, rebut_groups=rebut_groups)
# return render_template('index.html', user_input=original_rule_from_string, rule_strings=rule_from_string, rebut_groups=rebut_groups)
# return render_template('index.html', user_input=original_rule_from_string, rule_strings=original_new_arguments, rebut_groups=rebut_groups, all_defeats=all_defeats, all_attackers=all_attackers)
# Modify your route function to pass all_defeats as a dictionary
# return render_template('index.html', user_input=original_rule_from_string, rule_strings=original_new_arguments, rebut_groups=rebut_groups, all_defeats= {})



#TODO ajout l'affichage des undercut 
#TODO deplayer
#TODO faire le graphe
#TODO afficher les numero des argumentes A12: [r1]