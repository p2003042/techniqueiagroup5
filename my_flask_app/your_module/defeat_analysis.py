import matplotlib.pyplot as plt



def calculate_attacks(arguments):
    attacks = set()
    for arg_a in arguments:
        for arg_b in arguments:
            if arg_a != arg_b:
                if arg_b.top_rule.conclusion in arg_a.top_rule.premises:
                    attacks.add((arg_a.name, arg_b.name))
    return attacks


def generate_histogram(defeat_in_degree_counts):
    plt.figure(figsize=(10, 6))
    plt.bar(defeat_in_degree_counts.keys(), defeat_in_degree_counts.values(), color='skyblue')
    plt.xlabel('Defeat In-degree')
    plt.ylabel('Number of Arguments')
    plt.title('Histogram of Number of Arguments per Defeat In-degree')
    plt.xticks(list(defeat_in_degree_counts.keys()))
    plt.grid(axis='y', linestyle='--', alpha=0.7)
    plt.tight_layout()
    plt.show()