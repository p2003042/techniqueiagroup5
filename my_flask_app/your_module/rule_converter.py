import re
from rule_handler import Rule, Literal


#TODO faire en sorte que ca gere les poids des règles
def parse_user_input(user_input):
    rules = []
    lines = user_input.split('\n')
    for line in lines:
        line = line.strip()
        if not line:
            continue
        
        # Define regex pattern to match the rule format
        rule_pattern = re.compile(r'\[(?P<rule_name>[^\]]+)\]\s*(?P<premises>.+?)\s*(?P<arrow>->|=>)\s*(?P<conclusion>.+?)\s*(?P<rule_weight>\d+)?$')
        literal_pattern = re.compile(r'(!?)(\w+)')
        
        # Match the rule pattern
        match = rule_pattern.match(line)
        if not match:
            raise ValueError(f"Invalid rule format: {line}")
        
        rule_name = match.group('rule_name').strip()
        premise_str = match.group('premises').strip()
        conclusion_str = match.group('conclusion').strip()
        arrow = match.group('arrow')
        rule_weight = int(match.group('rule_weight') or '1')
        
        # Parse premises using literal_pattern
        premises = [Literal(p[1], p[0] == '!') for p in literal_pattern.findall(premise_str)]
        
        # Parse conclusion
        conclusion_parts = conclusion_str.split()
        conclusion = conclusion_parts[0]
        is_negative = conclusion.startswith('!')
        conclusion_literal = Literal(conclusion.lstrip('!'), is_negative)
        
        # Create rule object
        rule = Rule(premises, conclusion_literal, is_defeasible=True if arrow == '=>' else False, reference=rule_name)
        
        # Set rule weight
        rule.weight = rule_weight

        rules.append(rule)
        
    return rules


def serialize_rules(rules):
    serialized_rules = []
    for rule in rules:
        premises_str = ', '.join(['!' + premise.name if premise.is_negative else premise.name for premise in rule.premises])
        rule_str = f"[{rule.reference}] {premises_str}"
        rule_str += ' => ' if rule.is_defeasible else ' -> '
        rule_str += ('!' if rule.conclusion.is_negative else '') + rule.conclusion.name
        rule_str += f" {rule.weight}" if rule.weight != 1 else ""
        serialized_rules.append(rule_str)
    return '\n'.join(serialized_rules)



# Example usage:
# user_input = """
# [r1] -> a 1
# [r2] d,b => c 1
# [r3] !c -> d 1
# [r10] !c,b -> !d 1
# [r11] !c,d -> !b 1
# [r12] !d -> c 1
# [r4] a => !d 0
# [r5] => b 1
# [r6] => !c 1
# [r7] => d 0
# [r8] c => e 0
# [r9] !c => !r4 0
# """


def simpl_convert_test():
    parsed_rules = parse_user_input(user_input)
    for rule in parsed_rules:
        print(rule)
    print("-----------------------------")
    print(serialize_rules(parsed_rules))


# simpl_convert_test()
